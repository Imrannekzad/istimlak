@extends('Layout.index')


@section('content')
<!-- Content area -->

<div class="content d-flex justify-content-center align-items-center">

    <!-- Registration form -->
    <form action="{{route('users.store')}}" class="flex-fill" method="POST">
        @csrf
        <div class="row">
            <div class="col-lg-12">
                <div class="card mb-0">
                    <div class="card-body">
                        <div class="text-center mb-3">
                            <i class="icon-plus3 icon-2x text-success border-success border-3 rounded-round p-3 mb-3 mt-1"></i>
                            <h5 class="mb-0">Create account</h5>
                            <span class="d-block text-muted">All fields are required</span>
                        </div>

                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" placeholder="Choose username" name="username">
                            <div class="form-control-feedback">
                                <i class="icon-user-plus text-muted"></i>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="First name" name="fname">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="text" class="form-control" placeholder="Second name" name="lname">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-check text-muted"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                      

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <input type="email" class="form-control" placeholder="Your email" name="email">
                                    <div class="form-control-feedback">
                                        <i class="icon-mention text-muted"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-group-feedback form-group-feedback-right">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">+93</span>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Contact Number" name="contact_number">
                                    </div>
                                
                                    
                                    <div class="form-control-feedback">
                                        <i class="icon-phone2 text-muted"></i>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        <div class="form-group form-group-feedback form-group-feedback-right">
                            <input type="text" class="form-control" placeholder="job title/posation" name="posation">
                            <div class="form-control-feedback">
                                <i class="icon-user-plus text-muted"></i>
                            </div>
                        </div>
                        
                        
                
                        <div class="form-group col-sm-5">
                            <label></label>
                            <select data-placeholder="Select User Role" class="form-control select" name="role" data-fouc>
                                <option></option>
                                <optgroup>
                                    @foreach ($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option> 
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>

                        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-plus3"></i></b> Create account</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- /registration form -->

</div>
<!-- /content area -->

@endsection