
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

		<!-- Global stylesheets -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
		<link href="{{asset('global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/css/bootstrap_limitless.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/css/layout.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/css/components.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('assets/css/colors.min.css')}}" rel="stylesheet" type="text/css">
		<!-- /global stylesheets -->
	
		<!-- Core JS files -->
		<script src="{{asset('global_assets/js/main/jquery.min.js')}}"></script>
		<script src="{{asset('global_assets/js/main/bootstrap.bundle.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/loaders/blockui.min.js')}}"></script>
		<!-- /core JS files -->
	
		<!-- Theme JS files -->
		<script src="{{asset('global_assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/forms/selects/select2.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')}}"></script>
		<script src="{{asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
		<script src="{{asset('global_assets/js/demo_pages/form_select2.js')}}"></script>
		
	
		<script src="{{asset('assets/js/app.js')}}"></script>
		
	<script src="{{asset('global_assets/js/demo_pages/datatables_extension_buttons_print.js')}}"></script>
		<script src="{{asset('global_assets/js/demo_pages/datatables_extension_buttons_html5.js')}}"></script>
		<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	@include('Layout.header')
	<!-- /main navbar -->

					
	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		@include('Layout.sidebar')
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header">
				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
						<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
					</div>

				<!--
						<div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
						<div class="btn-group">
							<button type="button" class="btn bg-indigo-400"><i class="icon-stack2 mr-2"></i> New report</button>
							<button type="button" class="btn bg-indigo-400 dropdown-toggle" data-toggle="dropdown"></button>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="dropdown-header">Actions</div>
								<a href="#" class="dropdown-item"><i class="icon-file-eye"></i> View reports</a>
								<a href="#" class="dropdown-item"><i class="icon-file-plus"></i> Edit reports</a>
								<a href="#" class="dropdown-item"><i class="icon-file-stats"></i> Statistics</a>
								<div class="dropdown-header">Export</div>
								<a href="#" class="dropdown-item"><i class="icon-file-pdf"></i> Export to PDF</a>
								<a href="#" class="dropdown-item"><i class="icon-file-excel"></i> Export to CSV</a>
							</div>
						</div>
					</div>
				-->
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content pt-0">

				<!-- Main charts -->
				
				<!-- /main charts -->

					@yield('content')
				<!-- Dashboard content -->
			
				<!-- /dashboard content -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			@include('Layout.footer')
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>
