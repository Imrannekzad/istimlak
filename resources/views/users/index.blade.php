@extends('Layout.index')

@section('content')



	<!-- Basic initialization
    
   
    -->
    <div class="card">
      

        <div class="card-header header-elements-inline">
            <h5 class="card-title">Basic initialization</h5>
            <div class="header-elements">
            
            </div>
        </div>

        <div class="card-body">
            The HTML5 export buttons plug-in for Buttons provides four export buttons: <code>copyHtml5</code> - copy to clipboard; <code>csvHtml5</code> - save to CSV file; <code>excelHtml5</code> - save to XLSX file (requires JSZip); <code>pdfHtml5</code> - save to PDF file (requires PDFMake). This example demonstrates these four button types with their default options. Please note that these button types may also use a Flash fallback for older browsers (IE9-).
        </div>					
        <table class="table datatable-button-html5-basic">

            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email </th>
                    <th>Role</th>
                    <th>status</th>
                    <th>Posation</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td>{{$user->username;}}</td>
                    <td><a href="#">{{$user->email;}}</a></td>
                    <td>  @foreach($user->getRoleNames() as $role)
                        {{ $role }}
                      @endforeach</td>
                    <td>@if ($user->status == 1)
                        <span class="badge badge-success">Active</span>
                   
                            
                        @else
                        <span class="badge badge-success bg-danger">inActive</span>  
                        @endif
                    </td>
                    <td>{{$user->posation;}}</td>
                    <td>
                        <div class="list-icons">
         
                            <a href="{{ route('users.show', $user->id)}}" class="list-icons-item text-teal-600"><i class="icon-eye8"></i></a>
                            <a href="{{ route('users.edit', $user->id)}}" class="list-icons-item text-primary-600"><i class="icon-pencil7"></i></a>
                            
                            <a onclick="confirmDelete({{ $user->id }})" href="#" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                          

                            <form id="delete{{$user->id}}" action="{{ route('users.destroy', $user->id)}}" method="post">
                              @csrf
                              @method('DELETE')
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
             
             
            </tbody>
        </table>
    </div>
    
<script>
    function confirmDelete(id) {
      let text = 'آیا مطمین استید تا این ریکارد حذف شود؟';
      if (confirm(text) == true) {
        $('form#delete'+id).submit();
    
      } else {
        alert('ریکارد حذف نشد.');
      }
    
    }
    </script>
    
    <!-- /basic initialization -->

@endsection