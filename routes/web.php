<?php

use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/app', function () {
    return view('Layout.index');
})->middleware('auth')->name('dashboard');
Route::get('/test', function () {
    return view('test.index');
});

Route::post('/login',[\App\Http\Controllers\LoginController::class, 'login'])->name('login');

Route::get('/logout',[\App\Http\Controllers\LoginController::class, 'logout'])->name('logout');

Route::resource('users', UserController::class);
Route::resource('roles', RoleController::class);

Route::get('/login', function () {
    return view('login');
});
