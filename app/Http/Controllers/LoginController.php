<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
     public function login(Request $request)
    {
       
        
        if( Auth::attempt($request->only('username','password')) && Auth::user()->status == '1') {
           
           $request->session()->put('username', Auth::user()->username);
           $request->session()->put('id', Auth::user()->id);
           $request->session()->put('name', Auth::user()->name);
           $request->session()->put('email', Auth::user()->email);
            #return Auth::user();
            return redirect('app');
        }
        else {
            return "login false";
        }
    }
    public function logout()
    {
        session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }
}
