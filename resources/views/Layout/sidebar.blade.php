<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">
        
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="global_assets/images/placeholders/placeholder.jpg" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{ session('name')}}</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-envelop5 font-size-sm"></i> &nbsp;{{ session('email')}}
                        </div>
                    </div>

                    <div class="ml-3 align-self-center">
                        <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        
        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="index.html" class="nav-link active">
                        <i class="icon-home4"></i>
                        <span>
                            Dashboard
                            <span class="d-block font-weight-normal opacity-50">No active orders</span>
                        </span>
                    </a>
                </li>
            
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-city"></i> <span>Vertical navigation</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Vertical navigation">
                        <li class="nav-item"><a href="navigation_vertical_collapsible.html" class="nav-link">Collapsible menu</a></li>
                       <li class="nav-item"><a href="navigation_vertical_disabled.html" class="nav-link">Disabled items</a></li>
                    </ul>
                </li>
                <li class="nav-item"><a href="../../../RTL/default/full/index.html" class="nav-link"><i class="icon-width"></i> <span>RTL version</span></a></li>
                <!-- /main -->

                <!-- Layout -->
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Users</div> <i class="icon-menu" title="Layout options"></i></li>
           
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-users4"></i> <span>User Management</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="User Management">
                         <li class="nav-item"><a href="{{route('users.index')}}" class="nav-link"><i class="icon-user" title="Layout options"></i>View Users</a></li>
                        <li class="nav-item"><a href="{{route('users.create')}}" class="nav-link"><i class="icon-user-plus" title="Layout options"></i>Add Users</a></li>
                    </ul>
                </li>
                <li class="nav-item nav-item-submenu">
                    <a href="#" class="nav-link"><i class="icon-key"></i> <span>   Role Management</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Role Management">
                         <li class="nav-item"><a href="{{route('roles.index')}}" class="nav-link">View Roles</a></li>
                        <li class="nav-item"><a href="{{route('roles.create')}}" class="nav-link">Add New Role</a></li>
                    </ul>
                </li>
              
           
                <!-- /layout -->
                    
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->
    
</div>